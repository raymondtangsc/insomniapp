package com.Insomniapp.datacollector.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.Insomniapp.datacollector.Service.MonitorUserService;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 8/27/12
 * Time: 12:27 AM
 */
public class BootCompleteReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Intent serviceIntent = new Intent(context, MonitorUserService.class);
		context.startService(serviceIntent);
	}
}
