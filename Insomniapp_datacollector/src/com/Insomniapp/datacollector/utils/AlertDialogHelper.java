package com.Insomniapp.datacollector.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.Insomniapp.datacollector.R;

/**
 * Created with IntelliJ IDEA.
 * User: lior
 * Date: 19/7/12
 * Time: 2:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlertDialogHelper {

    public static void show(Context context, int messageId, int titleId){
        AlertDialogHelper.show(context, messageId, titleId, null);
    }

	public static void show(Context context, int messageId, int titleId, DialogInterface.OnClickListener onClickListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(messageId).
				setTitle(titleId).
				setPositiveButton(R.string.ok_button, onClickListener).
				show();
	}

	public static void confirm(Context context,
								int messageId,
								int titleId,
								int confirmButtonTextId,
								int declineButtonTextId,
								DialogInterface.OnClickListener onConfirmClickListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(messageId).
				setTitle(titleId).
				setPositiveButton(confirmButtonTextId, onConfirmClickListener).
				setNegativeButton(declineButtonTextId, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
					}
				}).
				show();
	}

}
