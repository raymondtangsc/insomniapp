package com.Insomniapp.datacollector.API;

import org.json.JSONException;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 9/7/12
 * Time: 2:45 AM
 */
public class User extends CachedModel {

	public static final int FEMALE = 0;
	public static final int MALE = 1;
	private static final String file_name ="user.bgd";

	private static User _user;
	private static int _age;
	private static int _gender;
	private static String AGE ="age";
	private static String GENDER="gender";

	protected User(){

	}

	public static User getCurrent(){
		if (_user == null){
			_user = new User();
			_user.loadFromCache();
		}
		return _user;
	}

	@Override
	String fileName() {
		return file_name;  //To change body of implemented methods use File | Settings | File Templates.
	}

	public void setAge(int age){
		try{
			getStorage().put(AGE,age);
		}catch (JSONException e){
		}
	}

	public boolean getIsFirstTime(){
		return !getStorage().has(AGE);
	}

	public void setGender(int gender){
		try{
			getStorage().put(GENDER,gender);
		}catch (JSONException e){

		}
	}

	public int getAge(){
		try{
		int age =  getStorage().getInt(AGE);
		return age;
		}
		catch (JSONException e){
		}
		return 0;
	}

	public int getGender(){
		try{
			int gender = getStorage().getInt(GENDER);
			return gender;
		}
		catch (JSONException e){
		}
		return 0;
	}





}
