package com.Insomniapp.datacollector.API;

import java.security.PublicKey;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 9/7/12
 * Time: 5:35 AM
 */
public class Moderators {

	public static final Float FEMALE_MODERATOR 		= 0.9F;
	public static final Float MALE_MODERATOR	 	= 1.1F;

	public static final Float DATETIME_7_TO_21_MODERATOR = 1.1F;
	public static final Float DATETIME_21_TO_7_MODERATOR = 0.05F;

	public static final Float AGE_MODERATOR = 0.01F;
}
