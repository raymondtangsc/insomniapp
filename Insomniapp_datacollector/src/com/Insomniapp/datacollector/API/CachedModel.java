package com.Insomniapp.datacollector.API;

import android.content.Context;
import org.json.JSONObject;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: lior
 * Date: 27/6/12
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class CachedModel {

	private JSONObject _storage;
	private static Context _applicationContext;

	protected CachedModel(){
		setStorage(new JSONObject());
	}

	public static void init(Context applicationContext){
		_applicationContext = applicationContext;
	}

	public void loadFromCache(){
		try{
			FileInputStream inputStream = _applicationContext.openFileInput(fileName());
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				total.append(line);
			}
			bufferedReader.close();
			inputStream.close();

			setStorage(new JSONObject(total.toString()));
		}
		catch (Exception e){
			setStorage(new JSONObject());
		}
	}

	public static void clearCache(){
		try{
			File cache = _applicationContext.getCacheDir();
			File appDir = new File(cache.getParent());
			if (appDir.exists()) {
				String[] children = appDir.list();
				for (String s : children) {
					if (!s.equals("lib")) {
						deleteDir(new File(appDir, s));
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public void saveToCache(){
		try{
			FileOutputStream outputStream = _applicationContext.openFileOutput(fileName(), Context.MODE_PRIVATE);
			outputStream.write(_storage.toString().getBytes());
			outputStream.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	protected JSONObject getStorage(){
		return _storage;
	}

	public void setStorage(JSONObject storage){
		_storage = storage;
		storageChanged();
	}

	abstract String fileName();

	protected void storageChanged(){
	}
}
