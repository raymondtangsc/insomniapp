package com.Insomniapp.datacollector.Activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.Insomniapp.datacollector.R;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 9/7/12
 * Time: 3:35 AM
 */
public class ReportTiredIndexScreen extends LinearLayout {

	private Context _context;
	private TextView _textTiredIndex;
	private IReport _callback;

	public ReportTiredIndexScreen(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public void setCallback(IReport callback){
		_callback = callback;
	}

	private void init(Context context) {
		inflate(context, R.layout.report_tired,this);

		_context = context;

		SeekBar tiredIndexSeekBar = (SeekBar)findViewById(R.id.tiredBar);
		_textTiredIndex = (TextView)findViewById(R.id.textTiredIndex);
		Button confirmationButton = (Button)findViewById(R.id.buttonRepotTiredIndexConfirm);

		tiredIndexSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
				if(i == 0) i = 1;
				_textTiredIndex.setText(Double.toString(i / 10.0));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				//To change body of implemented methods use File | Settings | File Templates.
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});

		confirmationButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				_callback.onConfirmButtonClick(Float.valueOf(_textTiredIndex.getText().toString()));
			}
		});

	}

	public void show() {
		setVisibility(VISIBLE);
	}

	public void hide() {
		setVisibility(INVISIBLE);
	}

	public interface IReport{
		void onConfirmButtonClick(float tiredIndex);
	}
}
