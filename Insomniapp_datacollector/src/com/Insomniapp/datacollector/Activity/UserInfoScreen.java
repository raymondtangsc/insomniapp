package com.Insomniapp.datacollector.Activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.Insomniapp.datacollector.API.User;
import com.Insomniapp.datacollector.R;
import com.Insomniapp.datacollector.utils.AlertDialogHelper;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 9/7/12
 * Time: 2:56 AM
 */
public class UserInfoScreen extends LinearLayout {

	private EditText _inputAge;
	private int _gender;
	private Context _context;
	private IUserInfo _callback;

	public UserInfoScreen(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public void setCallback(IUserInfo callback){
		_callback = callback;
	}

	private void init(Context context) {
		inflate(context, R.layout.user_info,this);

		_context = context;
		_inputAge = (EditText) findViewById(R.id.inputAge);
		final Button buttonMan = (Button)findViewById(R.id.buttonGenderMan);
		final Button buttonWomen = (Button)findViewById(R.id.buttonGenderWoman);
		Button buttonConfirm = (Button)findViewById(R.id.buttonUserInfoConfirm);

		buttonMan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				buttonMan.setSelected(true);
				buttonWomen.setSelected(false);
				_gender = User.MALE;
			}
		});

		buttonWomen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				buttonMan.setSelected(false);
				buttonWomen.setSelected(true);
				_gender = User.FEMALE;
			}
		});

		buttonConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(_inputAge.getText().length() == 0){
					AlertDialogHelper.show(_context,
							R.string.please_enter_age,
							R.string.age_requried,
							null);
				}else if(!buttonMan.isSelected() && !buttonWomen.isSelected()){
					AlertDialogHelper.show(_context,
							R.string.please_select_gender,
							R.string.gender_required,
							null);
				}else{
					User user = User.getCurrent();
					user.setAge(Integer.parseInt(_inputAge.getText().toString()));
					user.setGender(_gender);
					user.saveToCache();
					_callback.onUserInfoInputComplete();
				}
			}
		});
	}

	public void show() {
		setVisibility(VISIBLE);
	}

	public interface IUserInfo{
		void onUserInfoInputComplete();
	}
}
