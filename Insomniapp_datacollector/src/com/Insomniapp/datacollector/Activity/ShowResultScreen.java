package com.Insomniapp.datacollector.Activity;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.Insomniapp.datacollector.API.Moderators;
import com.Insomniapp.datacollector.API.User;
import com.Insomniapp.datacollector.R;

import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 9/7/12
 * Time: 4:52 AM
 */
public class ShowResultScreen extends LinearLayout {

	private Float _genderModerator;
	private Float _ageModerator;
	private Float _timeModerator;
	private Float _inputTiredModerator;

	private Float _result;

	private ImageView _resultPicture;

	public ShowResultScreen(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		inflate(context, R.layout.show_tiredness_result,this);

		_resultPicture = (ImageView) findViewById(R.id.imageResult);

	}

	public void calculateResult(float tiredIndex, float reponseIndex) {
		User user = User.getCurrent();

		_genderModerator = user.getGender() == User.FEMALE ? Moderators.FEMALE_MODERATOR : Moderators.MALE_MODERATOR;

		_inputTiredModerator = tiredIndex;

		int age = user.getAge();

		_ageModerator = 1 - Math.abs(25 - age)*Moderators.AGE_MODERATOR;

		int hour_of_day = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

		if(hour_of_day >= 7 && hour_of_day <= 21)
			_timeModerator = Moderators.DATETIME_7_TO_21_MODERATOR;
		else if(hour_of_day < 7)
			_timeModerator = Moderators.DATETIME_7_TO_21_MODERATOR - (hour_of_day + 3)*Moderators.DATETIME_21_TO_7_MODERATOR;
		else
			_timeModerator = Moderators.DATETIME_7_TO_21_MODERATOR - (hour_of_day - 21)*Moderators.DATETIME_21_TO_7_MODERATOR;

		//float randomModerator = Math.random()

		 _result = _genderModerator*_ageModerator*_inputTiredModerator*_timeModerator;

		if(_result <= 2.0){

		}else if(_result <= 4.0){
			_resultPicture.setBackgroundResource(R.drawable.result_scrn_2);
		}else if(_result <= 6.0){
			_resultPicture.setBackgroundResource(R.drawable.result_scrn_3);
		}else if(_result <= 8.0){
			_resultPicture.setBackgroundResource(R.drawable.result_scrn_4);
		}else{
			_resultPicture.setBackgroundResource(R.drawable.result_scrn_5);
		}
	}

	public void show() {
		setVisibility(VISIBLE);
	}

	public void hide() {
		setVisibility(INVISIBLE);
	}
}
