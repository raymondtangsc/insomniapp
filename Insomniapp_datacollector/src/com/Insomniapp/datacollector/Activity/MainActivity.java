package com.Insomniapp.datacollector.Activity;

import android.app.Activity;
import android.os.Bundle;
import com.Insomniapp.datacollector.API.CachedModel;
import com.Insomniapp.datacollector.API.User;
import com.Insomniapp.datacollector.R;

public class MainActivity extends Activity implements ReportTiredIndexScreen.IReport,
		UserInfoScreen.IUserInfo,
		ResponsivenessTest.IResponsivenessTest{
	/**
	 * Called when the activity is first created.
	 */

	private UserInfoScreen _userInfoScreen;
	private ReportTiredIndexScreen _reportTiredIndexScreen;
	private ShowResultScreen _showResultScreen;
	private ResponsivenessTest _responseivenesstest;
	private Float _tiredIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		CachedModel.init(getApplicationContext());
		_userInfoScreen = (UserInfoScreen) findViewById(R.id.userInfoScreen);
		_reportTiredIndexScreen = (ReportTiredIndexScreen) findViewById(R.id.reportTiredIndexScreen);
		_showResultScreen = (ShowResultScreen) findViewById(R.id.showResultScreen);
		_responseivenesstest = (ResponsivenessTest) findViewById(R.id.response_test);


		_reportTiredIndexScreen.setCallback(this);
		_responseivenesstest.setCallback(this);

		User user = User.getCurrent();

		if(user.getIsFirstTime()){
			_userInfoScreen.setCallback(this);
			_userInfoScreen.show();
		}else{
			_reportTiredIndexScreen.show();
		}
	}

	@Override
	public void onConfirmButtonClick(float tiredIndex) {
		_tiredIndex = tiredIndex;
		_reportTiredIndexScreen.hide();
		_responseivenesstest.show();
	}

	@Override
	public void onUserInfoInputComplete() {
		_showResultScreen.hide();
		_reportTiredIndexScreen.show();
	}

	@Override
	public void onFinishTest(float responsivenessIndex) {
		_responseivenesstest.hide();
		_showResultScreen.calculateResult(_tiredIndex,responsivenessIndex);
		_showResultScreen.show();

	}
}
