package com.Insomniapp.datacollector.Activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.Insomniapp.datacollector.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by IntelliJ IDEA.
 * User: raymondtang
 * Date: 9/8/12
 * Time: 2:22 PM
 */
public class ResponsivenessTest extends LinearLayout {

	private TextView _text_test_title;
	private ArrayList<Integer> _shape_resource_array;
	private ArrayList<String> _shape_name_array;

	private ArrayList<ImageButton> _imageviews_array;
	private TextView _text_timer;
	private Timer _timer;
	private int _seconds;

	private int _number_of_test =0;
	private IResponsivenessTest _callback;


	public ResponsivenessTest(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public void setCallback(IResponsivenessTest callback){
		_callback = callback;
	}

	private void init(Context context) {
		inflate(context, R.layout.response_test, this);

		_text_test_title = (TextView) findViewById(R.id.testTtile);
		_text_timer =(TextView) findViewById(R.id.text_timer);

		_timer= new Timer();

		initImageViews();
		initShapes();
	}

	private void initImageViews() {
		ImageButton _image_test_1 = (ImageButton) findViewById(R.id.image_test_shape_1);
		ImageButton _image_test_2 = (ImageButton) findViewById(R.id.image_test_shape_2);
		ImageButton _image_test_3 = (ImageButton) findViewById(R.id.image_test_shape_3);

		_imageviews_array = new ArrayList<ImageButton>();
		_imageviews_array.add(_image_test_1);
		_imageviews_array.add(_image_test_2);
		_imageviews_array.add(_image_test_3);

		for(int i=0; i< 3;i++){
			_imageviews_array.get(i).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					generateRandomTest();
				}
			});
		}
	}

	private void initShapes() {
		_shape_name_array = new ArrayList<String>();
		_shape_name_array.add("Green Circle");
		_shape_name_array.add("Red Circle");
		_shape_name_array.add("Yellow circle");

		_shape_name_array.add("Green Rectangular");
		_shape_name_array.add("Red Rectangular");
		_shape_name_array.add("Yellow Rectangular");

		_shape_name_array.add("Green Triangle");
		_shape_name_array.add("Red Triangle");
		_shape_name_array.add("Yellow Triangle");

		_shape_resource_array = new ArrayList<Integer>();

		_shape_resource_array.add(R.drawable.response_circle_grn);
		_shape_resource_array.add(R.drawable.response_circle_red);
		_shape_resource_array.add(R.drawable.response_circle_ylw);

		_shape_resource_array.add(R.drawable.response_rec_grn);
		_shape_resource_array.add(R.drawable.response_rec_red);
		_shape_resource_array.add(R.drawable.response_rec_ylw);

		_shape_resource_array.add(R.drawable.response_tri_grn);
		_shape_resource_array.add(R.drawable.response_tri_red);
		_shape_resource_array.add(R.drawable.response_tri_ylw);
	}

	public void show(){
		setVisibility(VISIBLE);

		_number_of_test = 0;

		generateRandomTest();
	}

	private void generateRandomTest() {

		if(_number_of_test > 5)
		{
			_callback.onFinishTest(generateRandomIntWithInRange(0,20)/10f);
			return;
		}

		startTimer();
		int choose_shape_number = generateRandomIntWithInRange(0,9);

		String title = "Tap the "+_shape_name_array.get(choose_shape_number)+" Fast";
		_text_test_title.setText(title);

		int random_shape_1,randome_shape_2;

		while (true){
			random_shape_1 = generateRandomIntWithInRange(0,9);

			if(random_shape_1 != choose_shape_number)
				break;
		}

		while (true){
			randome_shape_2 = generateRandomIntWithInRange(0,9);
			if((randome_shape_2 != random_shape_1) && (randome_shape_2 != choose_shape_number))
				break;
		};

		int correct_answer = generateRandomIntWithInRange(0,2);

		_imageviews_array.get(correct_answer).setBackgroundResource(_shape_resource_array.get(choose_shape_number));

		if(correct_answer == 0){
			_imageviews_array.get(1).setBackgroundResource(_shape_resource_array.get(random_shape_1));
			_imageviews_array.get(2).setBackgroundResource(_shape_resource_array.get(randome_shape_2));
		}

		if(correct_answer == 1) {
			_imageviews_array.get(0).setBackgroundResource(_shape_resource_array.get(random_shape_1));
			_imageviews_array.get(2).setBackgroundResource(_shape_resource_array.get(randome_shape_2));
		}

		if(correct_answer == 2){
			_imageviews_array.get(1).setBackgroundResource(_shape_resource_array.get(random_shape_1));
			_imageviews_array.get(0).setBackgroundResource(_shape_resource_array.get(randome_shape_2));
		}
		_number_of_test++;
		requestLayout();
	}

	private void startTimer() {
		if(_timer != null){
			_timer.cancel();
			_timer = new Timer();
		}
	 	_seconds =0;

		_timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				updateTimer();
			}

		},0,1000);

	}

	private void updateTimer() {

		post(new Runnable() {
			@Override
			public void run() {
				_text_timer.setText(Integer.toString(_seconds));
				_seconds++;
			}
		});
	}

	private int generateRandomIntWithInRange(int min,int max){
		return (int)(Math.random()*(max - min)) + min;
	}

	public  interface IResponsivenessTest{
		void onFinishTest(float responsivenessIndex);
	}

	public void hide(){
		setVisibility(GONE);
	}
}
