from google.appengine.ext import ndb


class Users(ndb.Model):
	user_name 	= ndb.StringProperty(required=True),
	full_name	= ndb.StringProperty(),
	email		= ndb.StringProperty(required = True),
	age 		= ndb.IntegerProperty(),
	gender		= ndb.StringProperty()


class BioClock(ndb.Model):
	record_date 		= ndb.StringProperty(required = True),
	user				= ndb.KeyProperty(kind=Users),
	timestamp 		 	= ndb.FloatProperty(required = True),
	state 			 	= ndb.StringProperty(choices = ["Wakeup","Sleep"], default= "Wakeup")

class ResponseSpeed(ndb.Model):
	record_date 		= ndb.StringProperty(required = True),
	user 				= ndb.KeyProperty(kind =Users),
	timestamp 			= ndb.FloatProperty(required = True),
	response_speed		= ndb.IntegerProperty(required = True)

class Tired(ndb.Model):
	record_date			= ndb.StringProperty(required = True),
	user 				= ndb.KeyProperty(kind = Users),
	timestamp 			= ndb.FloatProperty(required = True),
	tired_index			= ndb.IntegerProperty(required = True)
	