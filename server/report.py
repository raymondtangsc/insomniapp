import data
import logging
import framework
from google.appengine.ext import ndb

class ReportBioClockHandler(framework.Handler):

	def get(self):
		try:
			record_date,user_id,timestamp = self.get_report_header()

			state = self.request.get("state")
			self.check_request_parameter(timestamp,"state")

			record = data.BioClock(
				record_date 	= record_date,
				user 			= ndb.Key(data.Users, user_id),
				timestamp 		= timestamp,
				state 			= state)

			record.put()
			logging.info("recorded user bioclok for {0} with state {1}".format(user_id,state))
			self.successful_response()
		except Exception,e:
			self.digest_exception(e)

class ReportResponsivenessHandler(framework.Handler):
	
	def get(self):
		try:
			record_date,user_id,timestamp = self.get_report_header()

			response_speed = self.request.get("response_speed")
			self.check_request_parameter(response_speed,"response_speed")
			response_speed = int(response_speed)

			record = data.ResponseSpeed(
				record_date 	= record_date,
				user 			= ndb.Key(data.Users, user_id),
				timestamp 		= timestamp,
				response_speed 	= response_speed )

			record.put()
			logging.info("recorded response speed for {0} with speed {1}".format(user_id,response_speed))

			self.successful_response()
		except Exception,e:
			self.digest_exception(e)

class ReportTirednessHandler(framework.Handler):

	def get(self):
		try:
			record_date,user_id,timestamp = self.get_report_header()

			tired_index = self.request.get("tired_index")
			self.check_request_parameter(tired_index)
			tired_index = int(tired_index)

			record = data.Tired(
				record_date 	= record_date,
				user 			= ndb.Key(data.Users, user_id),
				timestamp 		= timestamp,
				tired_index 	= tired_index )
			record.put()

			logging.info("recorded tiredness of {0} with {1} tired index".format(user_id,tired_index))
		except Exception,e:
			self.digest_exception(e)



