import data
import framework
import logging
from google.appengine.ext import ndb

class RegisterHandler(framework.Handler):

	def get(self):
		try:
			user_name = self.request.get("user_name")
			self.check_request_parameter(user_name, "user_name")

			full_name = self.request.get("full_name")

			email = self.request.get("email")
			self.check_request_parameter(email,"email")

			age = self.request.get("age")
			gender = self.request.get("gender")

			user_key = ndb.Key(data.Users, email)
			user = user_key.get()

			if user is None:
				user = data.Users(
					id 			= email,
					user_name 	= user_name,
					full_name 	= full_name,
					email 		= email,
					age 		= age,
					gender 		= gender)
				
				user.put()
				logging.info("Registered User {0}".format(user_name))

			self.successful_response()
		except Exception, e:
			self.digest_exception(e)




