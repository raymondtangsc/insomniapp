import webapp2
import user
import report

api_v1 = '/api/v1/'
app = webapp2.WSGIApplication(
	[
		webapp2.Route(api_v1 + 'register', handler=user.RegisterHandler, name='register'),
		webapp2.Route(api_v1 + 'report/bioclock', handler = report.ReportBioClockHandler, name = 'report-bioclock'),
		webapp2.Route(api_v1 + 'report/responsiveness', handler = report.ReportResponsivenessHandler, name ='report-responsiveness'),
		webapp2.Route(api_v1 + 'report/tiredness', handler = report.ReportTirednessHandler, name ='report-tiredness')	
	],
	debug = True)