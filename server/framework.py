import webapp2
import logging
import data

from django.utils import simplejson as json


class RegistrationException(Exception):
	def __init__(self, message):
		Exception.__init__(self, message)


class BusinessException(Exception):
	def __init__(self, code, message):
		Exception.__init__(self, message)
		self._code = code	

	def code(self):
		return self._code;

class Handler(webapp2.RequestHandler):

	def build_response(self, code, message, **response):
		meta = {}
		meta["code"] = code
		meta["message"] = message
		reply = {}
		reply["meta"] = meta
		reply["response"] = response

		self.response.out.write(json.dumps(reply))

	def failed_response(self, code, message):
		self.build_response(code, message)

	def successful_response(self, **response):
		self.build_response(200, "successful", **response)

	def digest_exception(self, e):
		if isinstance(e, AuthenticationException):
			logging.error(e.message)
			self.response.set_status(401, e.message)
			self.failed_response(401, e.message)
		elif isinstance(e, BusinessException):
			logging.warn(e.message)
			self.response.set_status(e.code(), e.message)
			self.failed_response(e.code(), e.message)
		else:
			logging.exception(e)
			self.response.set_status(500, e.message)
			self.failed_response(500, e.message)

	def check_request_parameter(self, value, name):
		if not value:
			raise BusinessException(431, "Missing request parameter '{0}'".format(name))

	def get_report_header(self):
		record_date = self.request.get("record_date")
		self.check_request_parameter(record_date,"record_date")

		user_id = self.request.get("email")
		self.check_request_parameter(user_id,"email")

		timestamp = self.request.get("timestamp")
		self.check_request_parameter(timestamp,"timestamp")
		timestamp = float(timestamp)

		return record_date,user_id,timestamp

def to_bool(value):
    """
       Converts 'something' to boolean. Raises exception for invalid formats
           Possible True  values: 1, True, "1", "TRue", "yes", "y", "t"
           Possible False values: 0, False, None, [], {}, "", "0", "faLse", "no", "n", "f", 0.0, ...
    """
    if str(value).lower() in ("yes", "y", "true",  "t", "1"): return True
    if str(value).lower() in ("no",  "n", "false", "f", "0", "0.0", "", "none", "[]", "{}"): return False
    raise Exception('Invalid value for boolean conversion: ' + str(value))

